
import 'dart:convert';

import 'package:ema_project/Models/Users.dart';
import 'package:ema_project/pages/Login/LoginVM.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'RestClient.g.dart';

@RestApi(baseUrl: "http://nhatbaleplayboy1-001-site1.etempurl.com/")
abstract class RestClient {
  //factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  factory RestClient(Dio dio, {String baseUrl}){

    dio.options = BaseOptions(receiveTimeout: 5000, connectTimeout: 5000);
    // Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      // String token = await Users().token;
      // Do something before request is sent
      // print(token);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      if (prefs.getString("token").isNotEmpty) {
        options.headers["Authorization"] = "Bearer " + prefs.getString("token");
        print("Đã nhận token");
        return options;
      }
    },onResponse:(Response response) {
      // Do something with response data
      return response; // continue
    },onError: (DioError error) async {

    }
    ));
    return _RestClient(dio, baseUrl: baseUrl);}
  // @POST("/api/AuthManagement/Login")
  // Future<List<Users>> getUsers();
  @POST("/api/AuthManagement/Login")
  Future<Users> login(@Body() Map<String, dynamic> map);
}


  // @GET("/new")
  // Future<List<News>> getNewAll();
  // @GET("/new/Get5View")
  // Future<List<News>> getNews();
  // @GET("/new/{id}")
  // Future<News> getNewId(@Path("id") String id);
  // @GET("/QuestionHolland")
  // Future<List<QuestionHolland>> getQuestionHolland();
  // @POST("/user/login")
  // Future<LoginModel> login(@Body() Map<String, dynamic> map);

