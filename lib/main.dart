import 'package:after_layout/after_layout.dart';
import 'package:ema_project/pages/Home/HomePage.dart';
import 'package:ema_project/pages/StartView/Start.dart';
import 'package:ema_project/pages/StartView/StartHome.dart';
import 'package:ema_project/services/RestClient.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:provider_architecture/_viewmodel_provider.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Models/Users.dart';
import 'pages/Login/LoginVM.dart';
GetIt locator = GetIt.instance;
void main() {
  setupLocator();
  runApp(MyApp());
}
Future<void> setupLocator() async {
  //locator.registerSingleton<AppModel>(AppModel());
  locator.registerLazySingleton<RestClient>(
        () => RestClient(Dio()),
  );
}


class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,

      routes: {
        '/':(context){
          return MyHomePage();
        }
      },
    );
  }
}

class MyHomePage extends StatefulWidget {


  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".


  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with
      AutomaticKeepAliveClientMixin<MyHomePage>,
      AfterLayoutMixin<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }
  bool leuleu = false;
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    return ViewModelProvider<LoginVM>.withConsumer(
      onModelReady: (model) async {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        // model.testAPI(); Hàm chạy login
        // model.logoutAPI(); Hàm logout
        print(prefs.getString('token'));
        leuleu = prefs.getString('token').isNotEmpty;
        print('READY');

      },
      viewModelBuilder: () => LoginVM(),
      builder: (context, model, _) => Scaffold(
        body: leuleu ? StartHome() : Start(),
      ),
    );
  }

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
