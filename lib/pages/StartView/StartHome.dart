import 'package:ema_project/pages/Home/HomePage.dart';
import 'package:ema_project/pages/Login/Login.dart';
import 'package:ema_project/pages/Login/LoginVM.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class StartHome extends StatefulWidget {
  const StartHome({Key key}) : super(key: key);

  @override
  _StartHomeState createState() => _StartHomeState();
}

class _StartHomeState extends State<StartHome> {
  @override
  void initState() {
    Future.delayed(Duration(seconds: 3), (){
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> HomePage()));

    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          alignment: Alignment.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Lottie.asset('assets/lottie/38739-trackuni.json'),
              Text("EasyMom",style: TextStyle(fontSize: 36.0,fontWeight: FontWeight.bold,color: Colors.green),),
              Text("THEO DÕI SỰ PHÁT TRIỂN CỦA TRẺ NHỎ",style: TextStyle(fontSize: 16.0,fontWeight: FontWeight.normal,color: Colors.lightGreen))
            ],
          ),
        )

    );
  }
}
