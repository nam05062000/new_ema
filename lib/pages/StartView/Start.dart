import 'package:ema_project/pages/Login/Login.dart';
import 'package:ema_project/pages/Login/LoginVM.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class Start extends StatefulWidget {
  const Start({Key key}) : super(key: key);

  @override
  _StartState createState() => _StartState();
}

class _StartState extends State<Start> {
  @override
  void initState() {
    Future.delayed(Duration(seconds: 3), (){
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> Login()));

    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          alignment: Alignment.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Lottie.asset('assets/lottie/38739-trackuni.json'),
              Text("EasyMom",style: TextStyle(fontSize: 36.0,fontWeight: FontWeight.bold,color: Colors.green),),
              Text("THEO DÕI SỰ PHÁT TRIỂN CỦA TRẺ NHỎ",style: TextStyle(fontSize: 16.0,fontWeight: FontWeight.normal,color: Colors.lightGreen))
            ],
          ),
        )

    );
  }
}
