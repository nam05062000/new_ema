

import 'package:ema_project/pages/Home/HomePage.dart';
import 'package:ema_project/pages/Login/Login.dart';
import 'package:ema_project/services/RestClient.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';

class LoginVM extends ChangeNotifier {
  // String token;
  Future<void> logoutAPI() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove("token");

  }
  void testAPI(){

  
    Map<String, dynamic> map = {
      "phoneNumber": "0968354148",
      "password": "Abc@123",
    };

    locator<RestClient>().login(map).then((value) async {

      SharedPreferences prefs = await SharedPreferences.getInstance();
      if(prefs.getString('token').isNotEmpty){
        HomePage();
      }
      print('YYY');
      Logger().i(value);
      print(value.token);
      this.isLoading = false;
      notifyListeners();
      await prefs.setString('token',value.token);
      print(prefs.getString('token'));
    }).catchError((error) {
      print(error);
      notifyListeners();
    });
  }
  bool isLoading = true;
}