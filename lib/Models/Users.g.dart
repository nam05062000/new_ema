// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Users.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Users _$UsersFromJson(Map<String, dynamic> json) {
  return Users(
    id: json['id'] as String,
    phoneNumber: json['phoneNumber'] as String,
    password: json['password'] as String,
    token: json['token'] as String
  );
}

Map<String, dynamic> _$UsersToJson(Users instance) => <String, dynamic>{
  'id': instance.id,
  'phoneNumber': instance.phoneNumber,
  'password': instance.password,
  'token': instance.token,
};
