import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'Users.g.dart';

@JsonSerializable()
class Users {
  @JsonKey(name: "id")
  String id;
  @JsonKey(name: "phoneNumber")
  String phoneNumber;
  @JsonKey(name: "password")
  String password;
  @JsonKey(name: "token")
  String token;

  Users({this.id,this.phoneNumber,this.password,this.token});

  factory Users.fromJson(Map<String, dynamic> json) => _$UsersFromJson(json);
  Map<String, dynamic> toJson() => _$UsersToJson(this);

}